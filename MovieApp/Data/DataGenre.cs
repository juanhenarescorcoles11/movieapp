﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApp.Data
{
    public class DataGenre
    {

        private static List<Genre> generos;

        public DataGenre()
        {
            generos = new List<Genre>(){
                new Genre(){
                    ID = 1,
                    title = "Mafia",
                    description = "Genero de Mafia"
                },
                new Genre(){
                    ID = 2,
                    title = "Crime",
                    description = "Genero de Crimen"
                },
                new Genre(){
                    ID = 3,
                    title = "Comedy",
                    description = "Genero de Comedia"
                }
            }; 
        }

        public static int GetMaxId()
        {
            int maxId;

            List<int> listaId = new List<int>();

            foreach (Genre item in generos)
            {
                listaId.Add(item.ID);
            };

            if (listaId.Count == 0)
            {
                maxId = 0;
            }
            else
            {
                maxId = listaId.Max();
            }

            return maxId;
        }

        public static Genre GetGenre(int id)
        {
            Genre genero = generos.Find(g => g.ID == id);
            return genero;
        }

        public static List<Genre> GetAll()
        {
            return generos;
        }

        public static void Add(Genre genero)
        {
            genero.ID = GetMaxId() + 1;
            generos.Add(genero);
        }
        
        public static void Remove(int id)
        {
            generos.Remove(generos.Find(g => g.ID == id));
        }

        public static void Edit(Genre genero)
        {
            int index = generos.IndexOf(generos.Find(p => p.ID == genero.ID));
            generos[index] = genero;
        }

        public static List<string> GetAllTitle()
        {
            List<string> titulos = new List<string>();

            foreach(Genre item in generos)
            {
                titulos.Add(item.title);
            }

            return titulos;

        }

        public static Genre GetGenreByTitle(string title)
        {
            Genre genero = generos.Find(g => g.title == title);
            return genero;
        }

    }
}