﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieApp.Data
{

    public class DataMovie
    {
        public DataMovie()
        {
            peliculas = new List<Movie>();
            peliculas.Add(new Movie()
            {
                ID = 0,
                Title = "Scarface",
                //Genre = DataGenre.GetGenre(3),
                ReleaseDate = new DateTime(1995, 5, 30),
                EstaEnCartelera = true,
                Price = 10
            });
            peliculas.Add(new Movie()
            {
                ID = 1,
                Title = "Pulp Fiction",
                //Genre = DataGenre.GetGenre(1),
                ReleaseDate = new DateTime(1995, 5, 30),
                EstaEnCartelera = true,
                Price = 20
            });
            peliculas.Add(new Movie()
            {
                ID = 2,
                Title = "Shutter Island",
                //Genre = DataGenre.GetGenre(2),
                ReleaseDate = new DateTime(1995, 5, 30),
                EstaEnCartelera = false,
                Price = 35
            });
        }

   
        private static List<Movie> peliculas;
        
        public static int GetMaxId()
        {
            int maxId;

            List<int> listaId = new List<int>();

            foreach (Movie item in peliculas)
            {
                listaId.Add(item.ID);
            };

            if (listaId.Count == 0)
            {
                maxId = 0;
            }
            else
            {
                maxId = listaId.Max();
            }

            return maxId;
        }

        public static void AddMovie(Movie pelicula)
        {
            //Genre genero = DataGenre.GetGenreByTitle(pelicula.Genre.title);
            //pelicula.Genre = genero;
            pelicula.ID = GetMaxId() + 1;
            peliculas.Add(pelicula);
        }

        public static void DeleteMovie(int id)
        {
            Movie pelicula = DataMovie.GetMovie(id);
            peliculas.Remove(pelicula);
        }

        public static List<Movie> GetAll()
        {
            return peliculas;
        }

        public static Movie GetMovie(int id)
        {
            Movie pelicula = peliculas.Find(p => p.ID == id);
            return pelicula;
        }

        public static void Edit(Movie pelicula)
        {
            //Genre genero = DataGenre.GetGenreByTitle(pelicula.Genre.title);
            //pelicula.Genre = genero;
            int index = peliculas.IndexOf(peliculas.Find(p => p.ID == pelicula.ID));
            peliculas[index] = pelicula;
        }   
    }
}