﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieApp.Data
{

    public class DataMovieEF
    {
        private MovieDbContext db;

        DataMovieEF()
        {
            db = new MovieDbContext();
        }

        public void Add(Movie pelicula)
        {
            // pelicula.Genre = db.Genres.FirstOrDefault(x => x.title == pelicula.Genre.title);
            db.Movies.Add(pelicula);
            db.SaveChanges();
        }

        public void Remove(Movie pelicula)
        {
            db.Movies.Remove(pelicula);
            db.SaveChanges();
        }

        public List<Movie> GetAll()
        {
            List<Movie> peliculas = db.Movies.ToList();
            return peliculas;
        }

        public Movie GetMovie(int? id)
        {
            Movie pelicula = db.Movies.Find(id);
            return pelicula;
        }

        public void Edit(Movie pelicula)
        {
            db.Entry(pelicula).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            //Movie viejaPelicula = db.Movies.FirstOrDefault(x => x.ID == pelicula.ID);
            //viejaPelicula = pelicula;
            //db.SaveChanges();
        }
    }
}