﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieApp.Data
{

    public class DataGenreEF
    {

        private MovieDbContext db;

        public DataGenreEF()
        {
            db = new MovieDbContext();
        }

        public void Add(Genre genero)
        {
            db.Genres.Add(genero);
            db.SaveChanges();
        }

        public void Remove(Genre genero)
        {
            db.Genres.Remove(genero);
            db.SaveChanges();
        }

        public List<Genre> GetAll()
        {
            List<Genre> generos = db.Genres.ToList();
            return generos;
        }

        public List<string> GetTitleList()
        {
            List<string> generos = db.Genres.Select(x => x.title).ToList();
            return generos;
        }

        public Genre GetGenre(int id)
        {
            Genre genero = db.Genres.FirstOrDefault(x => x.ID == id);
            return genero;
        }

        public void Edit(Genre genero)
        {
            Genre viejoGenero = db.Genres.FirstOrDefault(x => x.ID == genero.ID);
            viejoGenero = genero;
            db.SaveChanges();

            // Otra forma
            //db.Entry(genero).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();
        }   
    }
}