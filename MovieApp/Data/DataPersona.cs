﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApp.Data
{
    public class DataPersona
    {
        private static List<Persona> _personas;

        public DataPersona()
        {
            _personas = new List<Persona>() {
                new Persona()
                {
                    id = 1,
                    age = 22,
                    name = "Juan",
                    email = "a@a.com",
                    surname = "Henares"
                },
                new Persona()
                {
                    id = 2,
                    age = 45,
                    name = "Andres",
                    email = "b@b.com",
                    surname = "Iniesta"
                },
                new Persona()
                {
                     id = 3,
                     age = 30,
                     name = "Maria",
                     email = "c@c.com",
                     surname = "Plaza"
                }
            };
        }

        public static List<Persona> GetAll()
        {
            return _personas;
        }

        public static void CrearPersona(Persona persona)
        {
            persona.id = GetMaxId();
            _personas.Add(persona);
        }

        public static void RemovePersona(Persona persona)
        {
            _personas.Remove(persona);
        }

        public static int GetMaxId()
        {
            int maxId;

            List<int> listaId = new List<int>();

            foreach (Persona item in _personas)
            {
                listaId.Add(item.id);
            };

            if (listaId.Count == 0)
            {
                maxId = 0;
            }
            else
            {
                maxId = listaId.Max();
            }

            return maxId;
        }

    }
}