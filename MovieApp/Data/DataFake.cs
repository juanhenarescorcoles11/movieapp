﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApp.Data
{
    public class DataFake
    {
        public static void SeedData()
        {
            new DataGenre();
            new DataMovie();
            new DataPersona();
            new DataMovieEF();
            new DataGenreEF();
        }
    }
}