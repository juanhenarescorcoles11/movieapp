﻿using MovieAppDAL.Data;
using MovieAppDAL.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MovieApp.Controllers
{
    public class GenreController : Controller
    {

        private DataGenre _datagenre;

        public GenreController()
        {
            _datagenre = new DataGenre();
        }

        // GET: Genre

        public ActionResult Index()
        {
            List<Genre> generos = _datagenre.GetAll();
            return View(generos);
        }

        // GET: Edit

        public ActionResult Edit(int id)
        {
            Genre genero = _datagenre.GetGenre(id);
            return View(genero);
        }

        // POST: Edit

        [HttpPost]
        public ActionResult Edit(Genre genero)
        {
            _datagenre.Edit(genero);
            return RedirectToAction("Index");
        }

        // GET: Create

        public ActionResult Create()
        {
            return View();
        }

        // POST: Create

        [HttpPost]
        public ActionResult Create(Genre genero)
        {
            _datagenre.Add(genero);
            return RedirectToAction("Index");
        }

        // GET: Details

        public ActionResult Details(int id)
        {
            Genre genero = _datagenre.GetGenre(id);
            return View(genero);
        }

        // GET: Delete

        public ActionResult Delete(int id)
        {
            _datagenre.Remove(id);
            return RedirectToAction("Index");
        }
    }
}