﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Reflection;
//using System.Web;
//using System.Web.Mvc;
//using MovieAppDAL.Data;
//using MovieAppDAL.Models;
//using Ninject;

//namespace MovieApp.Controllers
//{
//    public class MovieEFController : Controller
//    {

//        private IRepository<Movie> _mrepository;
//        private IRepository<Genre> _grepository;

//        public MovieEFController(IRepository<Movie> movierepository, IRepository<Genre> genrerepository)
//        {
//            var kernel = new StandardKernel();
//            kernel.Load(Assembly.GetExecutingAssembly());
//            _mrepository = kernel.Get<IRepository<Movie>>();
//            _grepository = kernel.Get<IRepository<Genre>>();
//        }

//        // GET: MovieEF
//        public ActionResult Index()
//        {
//            return View(_mrepository.GetAll());
//        }

//        // GET: MovieEF/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Movie movie = _mrepository.GetOne(id);
//            if (movie == null)
//            {
//                return HttpNotFound();
//            }
//            return View(movie);
//        }

//        // GET: MovieEF/Create
//        public ActionResult Create()
//        {
//            ViewBag.GenreID = new SelectList(_grepository.GetAll(), "ID", "title");
//            return View();
//        }

//        // POST: MovieEF/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "ID,Title,ReleaseDate,GenreID,Price,EstaEnCartelera")] Movie movie)
//        {
//            if (ModelState.IsValid)
//            {
//                _mrepository.Add(movie);
//                return RedirectToAction("Index");
//            }

//            ViewBag.GenreID = new SelectList(_grepository.GetAll(), "ID", "title", movie.GenreID);
//            return View(movie);
//        }

//        // GET: MovieEF/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Movie movie = _mrepository.GetOne(id);
//            if (movie == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.GenreID = new SelectList(_grepository.GetAll(), "ID", "title", movie.GenreID);
//            return View(movie);
//        }

//        // POST: MovieEF/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "ID,Title,ReleaseDate,GenreID,Price,EstaEnCartelera")] Movie movie)
//        {
//            if (ModelState.IsValid)
//            {
//                _mrepository.Edit(movie);
//                return RedirectToAction("Index");
//            }
//            ViewBag.GenreID = new SelectList(_grepository.GetAll(), "ID", "title", movie.GenreID);
//            return View(movie);
//        }

//        // GET: MovieEF/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Movie movie = _mrepository.GetOne(id);
//            if (movie == null)
//            {
//                return HttpNotFound();
//            }
//            return View(movie);
//        }

//        // POST: MovieEF/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            _mrepository.Remove(id);
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
                
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
