﻿using MovieAppDAL.Data;
using MovieAppDAL.Models;
using MovieAppDAL.Models.Alimentos;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieApp.Controllers
{
    public class PersonaController : Controller
    {

        private DataPersona _datapersona;

        private IAlimento _alimento;

        public PersonaController([Named("Pizza")]IAlimento alimento)
        {
            _alimento = alimento;
            _datapersona = new DataPersona();
        }

        public ViewResult Index()
        {
            List<Persona> personas = _datapersona.GetAll();
            return View(personas);
        }

        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CrearPersona(Persona persona)
        {
            BaseResultado _resultado = new BaseResultado();

            if (ModelState.IsValid)
            {
                _datapersona.CrearPersona(persona);
                _resultado.ok = true;
                _resultado.mensaje = "Persona creada";
            }
            else
            {
                _resultado.ok = false;
                _resultado.mensaje = "Error";
            }
            return Json(_resultado);
        }

        public ViewResult ListarAjax()
        {
            return View();
        }

        public PartialViewResult ListarAjaxDo()
        {
            // Buscar personas en BDD 
            var personas = _datapersona.GetAll();
            return PartialView("Index", personas);
        }

        public ViewResult ListarAjaxAvanzado()
        {
            return View();
        }

        public PartialViewResult ListarAjaxAvanzadoDo()
        {
            List<Persona> listado = _datapersona.GetAll();
            return PartialView("_ListadoAjaxDetalles", listado);
        }

        public PartialViewResult DetallePersona(string nombre)
        {
            var persona = _datapersona.GetAll().FirstOrDefault(x => x.Name == nombre);
            return PartialView("_DetallePersona", persona);
        }

        public JsonResult BuscarPersonas(string term)
        {
            var resultado = _datapersona.GetAll().Where(x => x.Name.Contains(term)).Select(x => x.Name).Take(5).ToList();
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ViewResult Autocompletar()
        {
            return View();
        }

        public JsonResult Comer()
        {
            var resultado = _alimento.Consumir();
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}
