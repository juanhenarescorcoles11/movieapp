﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MovieAppDAL.Data;
using MovieAppDAL.Models;

namespace MovieApp.Controllers
{
    public class GenreEFController : Controller
    {
        private IRepository<Genre> _grepository;

        public GenreEFController(IRepository<Genre> repository)
        {
            _grepository = repository;
        }

        // GET: GenreEF
        public ActionResult Index()
        {
            return View(_grepository.GetAll());
        }

        // GET: GenreEF/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Genre genre = _grepository.GetOne(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        // GET: GenreEF/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GenreEF/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,title,description")] Genre genre)
        {
            if (ModelState.IsValid)
            {
                _grepository.Add(genre);
                return RedirectToAction("Index");
            }

            return View(genre);
        }

        // GET: GenreEF/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Genre genre = _grepository.GetOne(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        // POST: GenreEF/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,title,description")] Genre genre)
        {
            if (ModelState.IsValid)
            {
                _grepository.Edit(genre);
                return RedirectToAction("Index");
            }
            return View(genre);
        }

        // GET: GenreEF/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Genre genre = _grepository.GetOne(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        // POST: GenreEF/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _grepository.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}
