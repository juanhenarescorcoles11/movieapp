﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using MovieAppBL.WebAPI;
using MovieAppDAL.Models;

namespace MovieApp.Controllers
{
    public class WebAPIController : Controller
    {

        // GET: WebAPI

        private IHttpClient client;
        private HttpFactory factory;

        public WebAPIController(HttpFactory pfactory)
        {
            factory = pfactory;
            client = factory.GetHttpClient("http://10.202.10.113:5000/ang/api/Cursos/");
        }

        public ActionResult Index()
        {
            return RedirectToAction("GetCursosAsync");
        }

        public ActionResult GetId()
        {
            return View();
        }

        public ActionResult Post()
        {
            return View();
        }

        public async Task<ActionResult> Delete(int id)
        {
            Curso curso = await client.GetId<Curso>(id); 
            return View(curso);
        }

        public async Task<ActionResult> Put(int id)
        {
            Curso curso = await client.GetId<Curso>(id);
            return View(curso);
        }

        public async Task<ActionResult> GetCursoAsync(int id)
        {
            Curso curso;

            try
            {
                curso = await client.GetId<Curso>(id);
            }

            catch (Exception ex)
            {
                if (ex is HttpRequestException)
                {
                    return Content("El Curso especificado es inválido");
                }            
                return Content("Error no controlado");
            }

            return View("_Details", curso);
        }

        public async Task<ActionResult> GetCursosAsync()
        {
            List<Curso> cursos = new List<Curso>();
            try
            {
                cursos = await client.Get<Curso>();
            }
            catch(Exception ex)
            {
                ViewBag.error = ex.Message;
            }
            
            return View("Listado", cursos);
        }

        [HttpPost]
        public async Task<ActionResult> CreateCursoAsync([Bind(Include = "name,descripcion,horas,max_alumnos,plazas_disponibles,precio")]Curso curso)
        {
            try
            {
                await client.Post<Curso>(curso);
                return RedirectToAction("GetCursosAsync");
            }
            catch(Exception ex)
            {
                ViewBag.error = ex.Message;
                return Json("No se ha podido crear el curso.");
            }
           
        }

        [HttpPost]
        public async Task<ActionResult> EditCursoAsync(Curso curso)
        {
            await client.Put<Curso>(curso, curso.id);
            return RedirectToAction("GetCursosAsync");
        }
        
        [HttpPost]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            await client.Delete(id);
            return RedirectToAction("GetCursosAsync");
        }

    }

}
