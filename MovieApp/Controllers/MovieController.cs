﻿using MovieAppDAL.Data;
using MovieAppDAL.Models;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using Ninject;
using System.Reflection;

namespace MovieApp.Controllers
{
    public class MovieController : Controller
    {
        private DataMovie _datamovie;
        private DataGenre _datagenre;

        public MovieController()
        {
            _datagenre = new DataGenre();
            _datamovie = new DataMovie(_datagenre);
        }

        // GET: Movie
        public ActionResult Index()
        {
            List<Movie> peliculas = _datamovie.GetAll();
            return View(peliculas);
        }
        
        public ActionResult Create()
        {
            ViewBag.miListado = _datagenre.GetAllTitle();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Movie pelicula)
        {
            _datamovie.AddMovie(pelicula);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            _datamovie.DeleteMovie(id);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewBag.miListado = _datagenre.GetAllTitle();
            Movie pelicula = _datamovie.GetMovie(id);
            return View(pelicula);
        }

        [HttpPost]
        public ActionResult Edit(Movie pelicula)
        {
            _datamovie.Edit(pelicula);
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            Movie pelicula = _datamovie.GetMovie(id);
            return View(pelicula);
        }

        public void GetFile()
        {
            List<Movie> peliculas = _datamovie.GetAll();

            string filespath = "~/Files";
 
            string path = Server.MapPath(filespath);

            string pathFull = path + "/fichero.txt";

            StringBuilder stringBuilder = new StringBuilder();

            foreach (Movie item in peliculas)
            {
                stringBuilder.AppendLine("ID: "+item.ID);
                stringBuilder.AppendLine("Titulo: "+item.Title);
                stringBuilder.AppendLine("Genero: "+item.Genre);
                stringBuilder.AppendLine("Precio: "+item.Price);
                stringBuilder.AppendLine("Fecha lanzamiento: "+item.ReleaseDate);
                stringBuilder.AppendLine("En cartelera: "+item.EstaEnCartelera);
            }

            System.IO.File.WriteAllText(pathFull , stringBuilder.ToString());

        }

        public JsonResult NumPar(int Price)
        {
            bool esValido = (Price % 2 == 0);
            return Json(esValido, JsonRequestBehavior.AllowGet);
        }
    }
}
