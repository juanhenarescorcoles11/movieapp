﻿using MovieApp.Models.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

namespace MovieApp.Models
{
    public class Persona
    {
        public int id { get; set; }

        [Display(ResourceType = typeof(PersonaResource), Name = "PersonaNameDisplay")]
        [StringLength(30, ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaNameStringLengthError")]
        public string name { get; set; }

        [Display(ResourceType = typeof(PersonaResource), Name = "PersonaSurnameDisplay")]
        [StringLength(50, ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaSurnameStringLengthError")]
        public string surname { get; set; }

        [Display(ResourceType = typeof(PersonaResource), Name = "PersonaEmailDisplay")]
        [EmailAddress(ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaEmailEmailAddressError")]
        public string email { get; set; }

        [NotMapped]
        [Compare("email", ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaEmailConfirmMatchError")]
        [Display(ResourceType = typeof(PersonaResource), Name="PersonaEmailConfirmDisplay")]
        public string emailconfirm { get; set; }

        [Display(ResourceType = typeof(PersonaResource), Name = "PersonaAgeDisplay")]
        [Range(0,150,ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaAgeRangeError")]
        public int age { get; set; }

        [CreditCard(ErrorMessageResourceType = typeof(PersonaResource),ErrorMessageResourceName = "PersonaCreditCardError" )]
        [Display(ResourceType = typeof(PersonaResource), Name = "PersonaCreditCardDisplay")]
        public string creditcard { get; set; }
    }
}