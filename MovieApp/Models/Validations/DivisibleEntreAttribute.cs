﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApp.Models.Validations
{
    public class DivisibleEntreAttribute : ValidationAttribute
    {

        private int _dividendo;
        private string mensajeDeError;

        public DivisibleEntreAttribute(int dividendo) : base("El campo {0} es inválido")
        {

            _dividendo = dividendo;

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if(value != null){ // Ya tenemos un Required, no queremos volver a implementar esa validacion aqui

                if((int)value % _dividendo != 0){

                    return new ValidationResult("El numero debe ser divisible entre "+ _dividendo);

                }

            }

            return ValidationResult.Success;

        }

    }
}