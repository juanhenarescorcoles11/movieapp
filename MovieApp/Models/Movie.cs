﻿using MovieApp.Models;
using MovieApp.Models.Resources;
using MovieApp.Models.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieApp.Models
{
    public class Movie : IValidatableObject

    {
        public int ID { get; set; }

        [Display(ResourceType = typeof(MovieResource), Name ="MovieTitleDisplay")]
        [Required(ErrorMessageResourceType = typeof(MovieResource), ErrorMessageResourceName = "MovieTitleRequiredError")]
        [StringLength(50,ErrorMessageResourceType = typeof(MovieResource),ErrorMessageResourceName = "MovieTitleStringLengthError")]
        public string Title { get; set; }

        public DateTime ReleaseDate { get; set; }

        //public int GenreID { get; set; }

        //[ForeignKey("GenreID")]
        //public Genre Genre { get; set; }

        [Range(15,100,ErrorMessageResourceType = typeof(MovieResource), ErrorMessageResourceName = "MoviePriceRangeError")]
        // Validacion Jqueryval
        [Remote("NumPar","Movie",ErrorMessageResourceType = typeof(MovieResource), ErrorMessageResourceName = "MoviePriceRemoteError")]
        // Validacion con clase
        [DivisibleEntre(5, ErrorMessage = "El campo {0} debe ser divisible entre {1}")] 
        public int Price { get; set; }

        public bool EstaEnCartelera { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errores = new List<ValidationResult>();
            if(Title == "Titulo prohibido")
            {
                // Se añade un ValidationResult al array de errores con dos parametros: 
                // 1 -Mensaje error validacion 
                // 2 -Array string con los campos que no se han validado
                errores.Add(new ValidationResult("No se puede utilizar el titulo 'Titulo prohibido'", new string[] { "Title" }));
            }

            return errores;
        }
    }

   
    public class Actor
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string apellidos { get; set; }
        public int edad { get; set; }
    }

    public class Genre
    {
        public int ID { get; set; }
        public string title { get; set; }
        public List<Movie> Movies { get; set; }
        public string description { get; set; }
    }

}
