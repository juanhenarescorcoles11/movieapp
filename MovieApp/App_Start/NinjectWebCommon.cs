[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MovieApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(MovieApp.App_Start.NinjectWebCommon), "Stop")]

namespace MovieApp.App_Start
{
    using System;
    using System.Reflection;
    using System.Web;
    using global::Ninject;
    using global::Ninject.Web.Common;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using MovieApp.Ninject;
    using MovieAppBL.WebAPI;
    using MovieAppDAL.Data;
    using MovieAppDAL.Models;
    using MovieAppDAL.Models.Alimentos;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                // kernel.Load(Assembly.GetExecutingAssembly());
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<HttpFactory>().To<HttpFactory>();
            kernel.Bind<IRepository<Movie>>().To<DataMovieEF>();
            kernel.Bind<IRepository<Genre>>().To<DataGenreEF>();
            kernel.Bind<IAlimento>().To<Hamburguesa>().Named("Hamburguesa");
            kernel.Bind<IAlimento>().To<Pizza>().Named("Pizza");
            kernel.Bind<IAlimento>().To<Paella>().Named("Paella");
        }        
    }
}
