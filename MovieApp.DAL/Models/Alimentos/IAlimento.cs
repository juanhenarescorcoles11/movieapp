﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieAppDAL.Models.Alimentos
{

    public interface IAlimento
    {
        string Consumir();
    }

    public class Hamburguesa : IAlimento
    {
        public string Consumir()
        {
            return "Has comido Hamburguesa";
        }
    }

    public class Pizza : IAlimento
    {
        public string Consumir()
        {
            return "Has comido Pizza";
        }
    }

    public class Paella : IAlimento
    {
        public string Consumir()
        {
            return "Has comido Paella";
        }
    }

}
