﻿namespace MovieAppDAL.Models
{
    public class Persona
    {
        public int Id { get; set; }

        //[Display(ResourceType = typeof(PersonaResource), Name = "PersonaNameDisplay")]
        //[StringLength(30, ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaNameStringLengthError")]
        public string Name { get; set; }

        //[Display(ResourceType = typeof(PersonaResource), Name = "PersonaSurnameDisplay")]
        //[StringLength(50, ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaSurnameStringLengthError")]
        public string Surname { get; set; }

        //[Display(ResourceType = typeof(PersonaResource), Name = "PersonaEmailDisplay")]
        //[EmailAddress(ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaEmailEmailAddressError")]
        public string Email { get; set; }

        //[NotMapped]
        //[Compare("email", ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaEmailConfirmMatchError")]
        //[Display(ResourceType = typeof(PersonaResource), Name="PersonaEmailConfirmDisplay")]
        public string Emailconfirm { get; set; }

        //[Display(ResourceType = typeof(PersonaResource), Name = "PersonaAgeDisplay")]
        //[Range(0,150,ErrorMessageResourceType = typeof(PersonaResource), ErrorMessageResourceName = "PersonaAgeRangeError")]
        public int Age { get; set; }

        //[CreditCard(ErrorMessageResourceType = typeof(PersonaResource),ErrorMessageResourceName = "PersonaCreditCardError" )]
        //[Display(ResourceType = typeof(PersonaResource), Name = "PersonaCreditCardDisplay")]
        public string Creditcard { get; set; }
    }
}