﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieAppDAL.Models
{
    public class Curso
    {
        public int id { get; set; }

        public string descripcion { get; set; }
      
        public Nullable<int> horas { get; set; }
       
        public Nullable<int> plazas_disponibles { get; set; }
       
        public Nullable<int> precio { get; set; }
       
        public Nullable<int> max_alumnos { get; set; }
        
        public string name { get; set; }
    }

}
