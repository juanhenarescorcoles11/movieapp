﻿using MovieAppDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieAppDAL.Data
{
    public class DataGenre
    {

        private static List<Genre> generos = new List<Genre>(){
                new Genre(){
                    ID = 1,
                    title = "Mafia",
                },
                new Genre(){
                    ID = 2,
                    title = "Crime",
                },
                new Genre(){
                    ID = 3,
                    title = "Comedy",
                }
            };

        public DataGenre()
        {

        }

        public int GetMaxId()
        {
            int maxId;

            List<int> listaId = new List<int>();

            foreach (Genre item in generos)
            {
                listaId.Add(item.ID);
            };

            if (listaId.Count == 0)
            {
                maxId = 0;
            }
            else
            {
                maxId = listaId.Max();
            }

            return maxId;
        }

        public Genre GetGenre(int id)
        {
            Genre genero = generos.Find(g => g.ID == id);
            return genero;
        }

        public List<Genre> GetAll()
        {
            return generos;
        }

        public void Add(Genre genero)
        {
            genero.ID = GetMaxId() + 1;
            generos.Add(genero);
        }
        
        public void Remove(int id)
        {
            generos.Remove(generos.Find(g => g.ID == id));
        }

        public void Edit(Genre genero)
        {
            int index = generos.IndexOf(generos.Find(p => p.ID == genero.ID));
            generos[index] = genero;
        }

        public List<string> GetAllTitle()
        {
            List<string> titulos = new List<string>();

            foreach(Genre item in generos)
            {
                titulos.Add(item.title);
            }

            return titulos;

        }

        public Genre GetGenreByTitle(string title)
        {
            Genre genero = generos.Find(g => g.title == title);
            return genero;
        }

    }
}