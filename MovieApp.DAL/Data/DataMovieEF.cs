﻿using MovieAppDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieAppDAL.Data
{

    public class DataMovieEF : IRepository<Movie>
    {
        private MovieDbContext db;

        public DataMovieEF()
        {
            db = new MovieDbContext();
        }

        public void Add(Movie pelicula)
        {
            db.Movies.Add(pelicula);
            db.SaveChanges();
        }

        public void Remove(int? id)
        {
            Movie pelicula = db.Movies.Find(id);
            db.Movies.Remove(pelicula);
            db.SaveChanges();
        }

        public List<Movie> GetAll()
        {
            List<Movie> peliculas = db.Movies.Include("Genre").ToList();
            return peliculas;
        }

        public Movie GetOne(int? id)
        {
            Movie pelicula = db.Movies.Include("Genre").FirstOrDefault(x => x.ID == id);
            return pelicula;
        }

        public void Edit(Movie pelicula)
        {
            db.Entry(pelicula).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
    }
}