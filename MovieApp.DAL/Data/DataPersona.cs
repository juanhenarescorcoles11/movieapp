﻿using MovieAppDAL.Models;
using MovieAppDAL.Models.Alimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieAppDAL.Data
{
    public class DataPersona
    {
        private static List<Persona> _personas = new List<Persona>() {
                new Persona()
                {
                    Id = 1,
                    Age = 22,
                    Name = "Juan",
                    Email = "a@a.com",
                    Surname = "Henares"
                },
                new Persona()
                {
                    Id = 2,
                    Age = 45,
                    Name = "Andres",
                    Email = "b@b.com",
                    Surname = "Iniesta"
                },
                new Persona()
                {
                     Id = 3,
                     Age = 30,
                     Name = "Maria",
                     Email = "c@c.com",
                     Surname = "Plaza"
                }
            };

        public List<Persona> GetAll()
        {
            return _personas;
        }

        public void CrearPersona(Persona persona)
        {
            persona.Id = GetMaxId();
            _personas.Add(persona);
        }

        public void RemovePersona(Persona persona)
        {
            _personas.Remove(persona);
        }

        public int GetMaxId()
        {
            int maxId;

            List<int> listaId = new List<int>();

            foreach (Persona item in _personas)
            {
                listaId.Add(item.Id);
            };

            if (listaId.Count == 0)
            {
                maxId = 0;
            }
            else
            {
                maxId = listaId.Max();
            }

            return maxId;
        }

        public string ConsumirAlimento(IAlimento alimento)
        {
            string mensaje = alimento.Consumir();
            return mensaje;
        }

    }
}