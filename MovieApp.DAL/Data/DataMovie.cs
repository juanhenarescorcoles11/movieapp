﻿using MovieAppDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieAppDAL.Data
{

    public class DataMovie
    {
        private DataGenre _datagenre;

        private static List<Movie>  peliculas = new List<Movie>(){
                new Movie()
                {
                    ID = 0,
                    Title = "Scarface",
                    Genre = new Genre(){ ID = 1, title = "Mafia" },
                    ReleaseDate = new DateTime(1995, 5, 30),
                    EstaEnCartelera = true,
                    Price = 10
                },
                new Movie()
                {
                    ID = 1,
                    Title = "Pulp Fiction",
                    Genre = new Genre(){ ID = 2, title = "Drama" },
                    ReleaseDate = new DateTime(1995, 5, 30),
                    EstaEnCartelera = true,
                    Price = 20
                },
                new Movie()
                {
                    ID = 2,
                    Title = "Shutter Island",
                    Genre = new Genre(){ ID = 3, title = "Drama" },
                    ReleaseDate = new DateTime(1995, 5, 30),
                    EstaEnCartelera = false,
                    Price = 35
                }
        };

        public DataMovie(DataGenre datagenre)
        {
            _datagenre = datagenre;
        }
    
        public int GetMaxId()
        {
            int maxId;

            List<int> listaId = new List<int>();

            foreach (Movie item in peliculas)
            {
                listaId.Add(item.ID);
            };

            if (listaId.Count == 0)
            {
                maxId = 0;
            }
            else
            {
                maxId = listaId.Max();
            }

            return maxId;
        }

        public void AddMovie(Movie pelicula)
        {
            Genre genero = _datagenre.GetGenreByTitle(pelicula.Genre.title);
            pelicula.Genre = genero;
            pelicula.ID = GetMaxId() + 1;
            peliculas.Add(pelicula);
        }

        public void DeleteMovie(int id)
        {
            Movie pelicula = GetMovie(id);
            peliculas.Remove(pelicula);
        }

        public List<Movie> GetAll()
        {
            return peliculas;
        }

        public Movie GetMovie(int id)
        {
            Movie pelicula = peliculas.Find(p => p.ID == id);
            return pelicula;
        }

        public void Edit(Movie pelicula)
        {
            Genre genero = _datagenre.GetGenreByTitle(pelicula.Genre.title);
            pelicula.Genre = genero;
            int index = peliculas.IndexOf(peliculas.Find(p => p.ID == pelicula.ID));
            peliculas[index] = pelicula;
        }   
    }
}