﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieAppDAL.Data
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        T GetOne(int? id);
        void Add(T obj);
        void Remove(int? id);
        void Edit(T obj);
    }
}
