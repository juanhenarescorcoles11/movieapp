﻿using MovieAppDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieAppDAL.Data
{

    public class DataGenreEF : IRepository<Genre>
    {

        private MovieDbContext db;

        public DataGenreEF()
        {
            db = new MovieDbContext();
        }

        public void Add(Genre genero)
        {
            db.Genres.Add(genero);
            db.SaveChanges();
        }

        public void Remove(int? id)
        {
            Genre genero = db.Genres.Find(id);
            db.Genres.Remove(genero);
            db.SaveChanges();
        }

        public List<Genre> GetAll()
        {
            List<Genre> generos = db.Genres.ToList();
            return generos;
        }

        public List<string> GetTitleList()
        {
            List<string> generos = db.Genres.Select(x => x.title).ToList();
            return generos;
        }

        public Genre GetOne(int? id)
        {
            Genre genero = db.Genres.Find(id);
            return genero;
        }

        public void Edit(Genre genero)
        {
            db.Entry(genero).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }   
    }
}