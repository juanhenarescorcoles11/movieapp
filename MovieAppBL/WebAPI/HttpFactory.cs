﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieAppBL.WebAPI
{
    public class HttpFactory
    {
        public MyHttpClient GetHttpClient(string urlbase)
        {
            return new MyHttpClient(urlbase);
        }
        public MyHttpWebRequest GetHttpWebRequest()
        {
            return new MyHttpWebRequest();
        }
        public MyRestSharp GetRestSharp()
        {
            return new MyRestSharp();
        }
    }
}
