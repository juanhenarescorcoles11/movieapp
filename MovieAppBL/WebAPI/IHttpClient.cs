﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieAppBL.WebAPI
{
    public interface IHttpClient
    {
        Task<List<T>> Get<T>();
        Task<T> GetId<T>(int id);
        Task<string> Post<T>(T o);
        Task<string> Put<T>(T o,int id);
        Task<string> Delete(int id);
    }
}
