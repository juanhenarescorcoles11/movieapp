﻿using MovieAppDAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace MovieAppBL.WebAPI
{
    public class MyHttpClient : IHttpClient
    {

        private HttpClient client;
        string urlbase;

        public MyHttpClient(string url)
        {
            urlbase = url;
            client = new HttpClient();
        }

        public async Task<string> Delete(int id)
        {
            string num = id.ToString();
            string url = urlbase + num;
            HttpResponseMessage response = await client.DeleteAsync(url);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<List<T>> Get<T>()
        {
            List<T> cursos = new List<T>();
            string jsonString = await client.GetStringAsync(urlbase);
            cursos = JsonConvert.DeserializeObject<List<T>>(jsonString);
            return cursos;
        }

        public async Task<T> GetId<T>(int id)
        {
            T curso;
            string url = urlbase + id.ToString();
            string jsonString = await client.GetStringAsync(url);
            curso = JsonConvert.DeserializeObject<T>(jsonString);
            return curso;
        }

        public async Task<string> Post<T>(T objeto)
        {
            T curso = objeto;
            HttpResponseMessage response = await client.PostAsJsonAsync(urlbase, curso);
            string respuesta = await response.Content.ReadAsStringAsync();
            return respuesta;
        }

        public async Task<string> Put<T>(T objeto, int id)
        {
            string url = urlbase + id;
            HttpResponseMessage response = await client.PutAsJsonAsync(url, objeto);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
